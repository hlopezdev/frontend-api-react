import * as React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";

import Posts from "./pages/Posts";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Posts />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
