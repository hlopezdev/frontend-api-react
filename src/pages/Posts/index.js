// import Pagination from "./components/Pagination";

import React, { useEffect, useState } from "react";

import { Link } from "react-router-dom";

const axios = require("axios").default;

const Posts = () => {
  const [data, setData] = useState([]);
  const [links, setLinks] = useState([]);
  const [page, setPage] = useState("http://127.0.0.1:8000/api/v2/posts");

  useEffect(() => {
    handleclick();
  }, [page]);

  const handleclick = async () => {
    axios
      .get(page)
      .then(function (response) {
        setData(response.data.data);
        setLinks(response.data.meta.links);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      })
      .then(function () {
        console.log(data);
        console.log(links);
      });
  };

  return (
    <div className="container mx-auto px-4 flex justify-center text-white flex-col">
      {/* <button
        onClick={handleclick}
        className="text-black bg-gray-400 w-20 h-10 border border-blue-300 rounded-md mx-auto mt-2"
      >
        Get Posts
      </button> */}
      <div className="grid grid-cols-3 my-10">
        {data.map((item, index) => {
          return (
            <div
              key={index}
              className="bg-gray-800 hover:bg-gray-700 border border-blue-300 p-5 rounded-md m-2"
            >
              <h2 className="font-bold text-lg mb-4">
                {item.id} - {item.post_name}
              </h2>
              <p className="text-xs">{item.content}</p>
              <p className="text-xs text-right">
                {item.author.name} - {item.created_at}
              </p>
              <p className="text-xs text-right">{item.author.email}</p>
            </div>
          );
        })}
      </div>
      <div className="bg-white px-4 py-3 flex items-center justify-between border-t border-gray-200 sm:px-6">
        <div className="hidden sm:flex-1 sm:flex sm:items-center sm:justify-end">
          <div className="flex justify-end">
            <nav
              className="relative z-0 inline-flex rounded-md shadow-sm -space-x-px"
              aria-label="Pagination"
            >
              {links.map((item, index) => {
                return (
                  <div key={index}>
                    <Link
                      to="/"
                      onClick={() => setPage(item.url)}
                      className={
                        item.active === true
                          ? "z-10 bg-indigo-50 border-indigo-500 text-indigo-600 relative inline-flex items-center px-4 py-2 border text-sm font-medium"
                          : "bg-white border-gray-300 text-gray-500 hover:bg-gray-50 relative inline-flex items-center px-4 py-2 border text-sm font-medium"
                      }
                    >
                      {item.label}
                    </Link>
                  </div>
                );
              })}
            </nav>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Posts;
